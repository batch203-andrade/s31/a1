/*
- What directive is used by Node.js in loading the modules it needs?
    (require)
- What Node.js module contains a method for server creation?
    (http)
- What is the method of the http object responsible for creating a server using Node.js?
    (createServer)
- What method of the response object allows us to set status codes and content types?
    (writeHead())
- Where will console.log() output its contents when run in Node.js?
    (Outside the server function)
- What property of the request object contains the address's endpoint?
    (url())
*/