let http = require('http');
let port = 3000;
let server = http.createServer;


server((req, res) => {
    if (req.url == "/login") {
        res.writeHead(200, { "Content-Type": "text/plain" });
        res.end("Hello Again");
    } else {
        res.writeHead(200, { "Content-Type": "text/plain" });
        res.end("The page you are looking for cannot be found.");
    }
})
    .listen(port);


console.log(`Listening on port ${port}`);

